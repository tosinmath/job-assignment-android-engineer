# Fishbrain Catches List

## Libraries used

* Google AppCompat Support Libraries
* Google Material Design Support Libraries
* Glide - for image loading
* Rxjava2 - for asynchronous request with observable streams
* Retrofit2 - type-safe REST client to make API request.
* Dagger - for dependency injection
* JUnit - for Unit testing
* Mockito - for creating a "mock" for classes
* Espresso - for User Interface testing
* Circleimageview - to make ImageView with round corners for User Images.

## This App has 2 screens
- First screen with content that is loaded dynamically from https://rutilus-staging.fishbrain.com.
     1. This screen contains a RecyclerView that shows content in a list with images with description.
     2. Each row in the list has a Like Button icon for Likes feature.
     3. Each row in the list has a Remove catch button to remove the catches he/she doesn't like.
     4. Displays a loadmore at the bottom whenever user scrolls to the bottom of the list, which displays catches by page id.
     5. Shows a snackbar message as Feedback when user clicks on "Like" or "Remove catch" button.

- In second screen, each catch can be seen in more detail.
    1. This screen contains a bigger image & description of the catch from the RecyclerView list.
    2. It contains detailed information for a catch.

## The catch list item can have the following information:
* User's profile image shows on the RecyclerView list.
* Description such as "Johan caught a Pike yesterday." and can be clickable.
* All Images with description.
* Like button on the RecyclerView list.

## Other things done.

* The project contain tests Unit Test and UI instrumentation test.
* The design/structure of the application code is production ready and extensible
* This Application is developed in Java
* API 15 as minSdkVersion.
* Fluid layout, that adapts to most phones and tablets.
* All labels, variable names, and identifiers of any kind in English.
* Code is documented.

# Challenges I faced and issues I met during the assignment
1. Support Library 23 too old for RecyclerView, So I updated the support Library
2. I had to update project to Java8 to support Lambda expression in my assignment.
3. I had to create a CallBack Interface so I could convinently call method from the View class for Like Button & Remve catch.
4. I was forced to import Dagger compiler to both Projects as It wasn't compiled on the main project "app" itself.
5. I discovered some of the Catch data could be NULL, so I had to properly include checks to solve that.

## Issues faced during testing.
1. I had to use Thread.sleep() for my Espresso test to delay till the RecyclerView UI is full loaded, so I could avoid NULL pointer exceptions.
2. I was getting an observeOn(AndroidSchedulers.mainThread()) Exception while running my Unit Test, so I had to use "RxAndroidPlugins" to avoid this issue so the default schedulers will be overridden.

I hope to get an opportunity to talk more about this and learn ways to solve these issues more efficiently.


## Architecture

I implemented Model View Presenter for this project, also known as MVP, the purpose of this is to break the application into modular, single purpose components. Thus encouraging separation of concerns.

This also follows the “SOLID” principles which makes the codebase scalable and more robust.


## Presenter Layer

I implemented Presenter classes which sole aim is to be the communicator between the View Layer and Model Layer, acting as a middle man between the two layers.

The Two Presenters (CatchesListpresenter and CatchesDetailPresenter) hosts logic to return formatted data from the Model back to the View for display to the user. So each Activity has a matching presenter that handles all access to the model. The presenters passes data to the Activities when the data is ready to display.


## View Layer
The view layer, also known as the UI layer which I named MainActivity and DetailActivity in my assignment, It has a reference to the presenter, which is instantiated in the view. The views have interfaces that the presenter interacts with. The main function of the View is to call methods in the Presenter class. The presenters then calls appropriate method in the Views interfaces when data is ready to display.


## Model Layer
The Model Layer to which has most been already implemented in the project under the Domain folder, It represent the source of the data that I wish to display on the View Layer. It has to do with fetching data via the *Repository Pattern*, in my case, I implemented **CatchesRepository** class for this purpose.

The ultimate goal is to expose an interface that defines the different queries that need to be performed in order to abstract away the type of data source used.

In order for presenters to be served specific related data only. I used Interactor which already created in the project which is **CatchesInteractorImpl** class for this purpose.

## Interactors

The interactors will fetch data from my **CatchesRepository** data source. After getting the data, the interactor will send the data to the presenter. Thus, making changes in the UI by calling the appropriate method in the Views interfaces when data is ready to display.

## Testing

* This projects unit tests using JUnit and mockito for mocking objects.
* This project has User Interface test using Espresso testing framework.

