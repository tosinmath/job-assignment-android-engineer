package com.fishbrain.android;


import android.support.annotation.NonNull;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.network.interactor.CatchesInteractor;
import com.fishbrain.assignment.ui.catcheslist.CatchesListPresenter;
import com.fishbrain.assignment.ui.catcheslist.CatchesView;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CatchesPresenterTest {

    @Mock
    CatchesInteractor catchesInteractor;

    @Mock
    CatchesListPresenter catchesListPresenter;

    @Mock
    CatchesView view;

    CompositeDisposable compositeDisposable;

    String pageId = "4539";

    @BeforeClass
    public static void setUpRxSchedulers() {
        Scheduler immediate = new Scheduler() {
            @Override
            public Disposable scheduleDirect(@NonNull Runnable run, long delay, @NonNull TimeUnit unit) {
                return super.scheduleDirect(run, 0, unit);
            }

            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run);
            }
        };

        RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitComputationSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitNewThreadSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitSingleSchedulerHandler(scheduler -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> immediate);
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        compositeDisposable = new CompositeDisposable();

        List<Catch> catches = new ArrayList<>();
        when(catchesInteractor.fetchCatches(pageId)).thenReturn(Observable.just(catches));

        catchesListPresenter = new CatchesListPresenter(catchesInteractor);
        catchesListPresenter.attachView(view);
    }

    @Test
    public void testDisplayCalled() {
        catchesListPresenter.getCatches(compositeDisposable, false, pageId);
        List<Catch> catches = new ArrayList<>();
        verify(view).setAdapter(catches, false);
        verify(view).showLoading();
    }

    @After
    public void tearDown() throws Exception {
        catchesListPresenter.detachView();
    }

}
