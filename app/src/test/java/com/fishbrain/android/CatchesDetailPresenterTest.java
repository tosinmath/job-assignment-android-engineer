package com.fishbrain.android;

import android.support.annotation.NonNull;

import com.fishbrain.android.domain.Avatar;
import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.domain.Owner;
import com.fishbrain.android.domain.Photo;
import com.fishbrain.android.domain.PhotoContainer;
import com.fishbrain.android.domain.RecentLikes;
import com.fishbrain.android.domain.Species;
import com.fishbrain.android.network.CalendarJsonDeserializer;
import com.fishbrain.android.network.interactor.CatchesInteractor;
import com.fishbrain.assignment.ui.catcheslist.CatchesListPresenter;
import com.fishbrain.assignment.ui.catcheslist.CatchesView;
import com.fishbrain.assignment.ui.details.CatchesDetailPresenter;
import com.fishbrain.assignment.ui.details.DetailView;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

import static junit.framework.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.domain.Owner;
import com.fishbrain.android.domain.PhotoContainer;
import com.fishbrain.android.domain.RecentLikes;
import com.fishbrain.android.domain.Species;
import com.fishbrain.android.network.service.ApiService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.subscribers.TestSubscriber;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static org.mockito.Mockito.when;

public class CatchesDetailPresenterTest {

    @Mock
    CatchesInteractor catchesInteractor;

    @Mock
    CatchesDetailPresenter catchesDetailPresenter;

    @Mock
    DetailView view;

    CompositeDisposable compositeDisposable;

    String pageId = "4539";

    Catch catches;

    DateFormat simpleDateFormat = new SimpleDateFormat(BuildConfig.DATE_FORMAT2, Locale.getDefault());

    @BeforeClass
    public static void setUpRxSchedulers() {
        Scheduler immediate = new Scheduler() {
            @Override
            public Disposable scheduleDirect(@NonNull Runnable run, long delay, @NonNull TimeUnit unit) {
                return super.scheduleDirect(run, 0, unit);
            }

            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run);
            }
        };

        RxJavaPlugins.setInitIoSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitComputationSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitNewThreadSchedulerHandler(scheduler -> immediate);
        RxJavaPlugins.setInitSingleSchedulerHandler(scheduler -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(scheduler -> immediate);
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        compositeDisposable = new CompositeDisposable();

        catches = getCatches();
        when(catchesInteractor.fetchCatch(pageId)).thenReturn(Observable.just(catches));

        catchesDetailPresenter = new CatchesDetailPresenter(catchesInteractor);
        catchesDetailPresenter.attachView(view);
    }

    @Test
    public void testDisplayCalled() throws Exception {

        catchesDetailPresenter.getCatch(compositeDisposable, pageId);

        // Verify if these methods were called
        verify(view).setCatchDetails(catches);
        verify(view).showLoading();
    }

    private Catch getCatches() throws Exception {
        // Setting the Catch data
        String type = "catch_verbose_social";
        int id = 0;
        String description = "Johan caught a Pike yesterday";

        // Parse dateCaught and dateCreated
        String dateCaughtStr = "1998-06-11T21:00:00.000Z";
        String dateCreatedStr = "1998-06-11T15:00:00.000Z";
        Calendar dateCaught =  parseDateString(dateCaughtStr);
        Calendar dateCreated = parseDateString(dateCreatedStr);

        double weight = 0.5;
        double length = 0.4;

        // Set Owner class values
        String nickname = "Fishmaster";
        String firstName = "Jensa";
        String lastName = "Persson";
        String countryCode = "SE";
        boolean featured = true;
        boolean isPremium = true;
        Avatar avatar = new Avatar(null); // No avatar provided
        Owner owner = new Owner(nickname, firstName, lastName,
                countryCode, dateCreated, featured, isPremium, avatar);

        // Set species value
        String mId = "360";
        String mName = "Pike-perch";
        String mSpecies = "Sander lucioperca";
        String mStatus = "native";
        String mImage = "https://cdn-staging.fishbrain.com/fish/360/original?1455719674";
        Photo photo = new Photo(null, null); // No photo given
        Species species = new Species(mId, mName, mSpecies, mStatus, mImage, photo);

        RecentLikes recentLikes = new RecentLikes();
        recentLikes.setTotalNumberOfLikes(10);
        recentLikes.setType("recent_likes");

        List<PhotoContainer> photoContainers = new ArrayList<>();

        Catch catches = new Catch(type, id, description, dateCaught,
                weight, length, dateCreated, owner, species, photoContainers, recentLikes);

        return catches;
    }

    @After
    public void tearDown() throws Exception {
        catchesDetailPresenter.detachView();
    }

    public Calendar parseDateString(String dateStr){
        Date date;
        Calendar cal = Calendar.getInstance();
        try {
            date = simpleDateFormat.parse(dateStr);
            cal.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return cal;
    }
}

