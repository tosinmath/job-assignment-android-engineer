package com.fishbrain.assignment.ui.catcheslist;


import android.support.annotation.NonNull;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.widget.TextView;

import static android.support.test.espresso.action.ViewActions.click;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.core.IsNot.not;

import com.fishbrain.assignment.R;

import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.core.deps.guava.base.Preconditions.checkNotNull;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.ExecutionException;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;


@LargeTest
@RunWith(AndroidJUnit4.class)
public class UserLikesTest {

    private IdlingResource idlingResource;
    private RecyclerView recyclerView;
    private int numberOfClicks = 2;

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void userLikesTest() throws InterruptedException, ExecutionException{

        Thread.sleep(4000);

        recyclerView = mActivityTestRule.getActivity().findViewById(R.id.catches_recyclerview);
        RecyclerView.ViewHolder viewHolder = recyclerView.findViewHolderForAdapterPosition(0);

        String currentLike = ((TextView) viewHolder.itemView.findViewById(R.id.likes_count)).getText().toString();
        int likeCount = Integer.parseInt(currentLike.substring(0, 1));

        // Click on the Like button Twice
       ViewInteraction recyclerView1 = onView(
                allOf(withId(R.id.catches_recyclerview), isDisplayed()));
        recyclerView1.perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.like_button)));

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.catches_recyclerview), isDisplayed()));
        recyclerView2.perform(actionOnItemAtPosition(0, clickChildViewWithId(R.id.like_button)));


        // calculate the expected value
        String expectedLikes = String.valueOf(likeCount + numberOfClicks) + " Likes";

        // check if text matches with expected Likes value after Two clicks
        onView(withId(R.id.catches_recyclerview)).check(matches(
                atPosition(0, hasDescendant(allOf(withId(R.id.likes_count), withText(expectedLikes))))));

    }

    public Matcher<View> atPosition(final int position, @NonNull final Matcher<View> itemMatcher) {
        checkNotNull(itemMatcher);
        return new BoundedMatcher<View, RecyclerView>(RecyclerView.class) {
            @Override
            public void describeTo(Description description) {
                description.appendText("has item at position " + position + ": ");
                itemMatcher.describeTo(description);
            }

            @Override
            protected boolean matchesSafely(final RecyclerView view) {
                RecyclerView.ViewHolder viewHolder = view.findViewHolderForAdapterPosition(position);
                if (viewHolder == null) {
                    // has no item on such position
                    return false;
                }
                return itemMatcher.matches(viewHolder.itemView);
            }
        };
    }

    public ViewAction clickChildViewWithId(final int id) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return null;
            }

            @Override
            public String getDescription() {
                return "Click on a child view with specified id.";
            }

            @Override
            public void perform(UiController uiController, View view) {
                View v = view.findViewById(id);
                v.performClick();
            }
        };
    }
}

