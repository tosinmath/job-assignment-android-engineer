package com.fishbrain.assignment.base;


import android.support.v4.app.Fragment;

/**
 * This is a base abstract fragment used for declaring the common methods for our fragments
 *
 * Created by dimitris.lachanas on 23/08/15.
 */
public abstract class BaseFragment extends Fragment {

}
