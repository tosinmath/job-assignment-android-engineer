package com.fishbrain.assignment.base;


public interface MvpView {

    void showLoading();

    void hideLoading();
}
