package com.fishbrain.assignment.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.fishbrain.assignment.R;
import com.fishbrain.assignment.dagger.component.CatchesListComponent;

/**
 * A base activity, for the common methods of our activities
 *
 * Created by dimitris.lachanas on 22/08/15.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private CatchesListComponent component;
    private Snackbar snackbarOffline;
    private Snackbar snackbarMsg;

    protected abstract void setupActivity(CatchesListComponent component, Bundle savedInstanceState);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActivity(component, savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    protected abstract void loadView();

    public void displayOfflineSnackbar() {
        snackbarOffline = Snackbar.make(findViewById(android.R.id.content), R.string.no_connection_snackbar, Snackbar.LENGTH_INDEFINITE);
        TextView snackbarText = (TextView) snackbarOffline.getView().findViewById(android.support.design.R.id.snackbar_text);
        snackbarText.setTextColor(getApplicationContext().getResources().getColor(android.R.color.white));
        snackbarOffline.setAction(R.string.snackbar_action_retry, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadView();
            }
        });
        snackbarOffline.setActionTextColor(getResources().getColor(R.color.colorPrimary));
        snackbarOffline.show();
    }

    public void hideOfflineSnackBar() {
        if (snackbarOffline != null && snackbarOffline.isShown()) {
            snackbarOffline.dismiss();
        }
    }

    public void showSnackbarMsg(@NonNull String msg) {
        snackbarMsg = Snackbar.make(findViewById(android.R.id.content), msg, Snackbar.LENGTH_SHORT);
        snackbarMsg.getView().setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        TextView snackbarText = (TextView) snackbarMsg.getView().findViewById(android.support.design.R.id.snackbar_text);
        snackbarText.setTextColor(getApplicationContext().getResources().getColor(android.R.color.white));
        snackbarMsg.setActionTextColor(getResources().getColor(R.color.colorPrimary));
        snackbarMsg.show();
    }

}
