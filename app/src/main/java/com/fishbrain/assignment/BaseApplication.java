package com.fishbrain.assignment;


import android.app.Application;
import android.content.Context;


import com.fishbrain.assignment.dagger.component.DaggerCatchesListComponent;
import com.fishbrain.assignment.dagger.component.DaggerNetComponent;
import com.fishbrain.assignment.dagger.module.AppModule;
import com.fishbrain.assignment.dagger.module.CatchesListModule;
import com.fishbrain.assignment.dagger.module.NetModule;
import com.fishbrain.assignment.dagger.module.RetrofitModule;
import com.fishbrain.assignment.dagger.component.CatchesListComponent;
import com.fishbrain.assignment.dagger.component.NetComponent;
import com.squareup.leakcanary.LeakCanary;
import com.squareup.leakcanary.RefWatcher;

public class BaseApplication extends Application {

    private NetComponent netComponent;
    private CatchesListComponent catchesListComponent;
    private RefWatcher refWatcher;

    @Override
    public void onCreate() {
        super.onCreate();

        netComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule())
                .build();

        catchesListComponent = DaggerCatchesListComponent.builder()
                .netComponent(netComponent)
                .retrofitModule(new RetrofitModule())
                .catchesListModule(new CatchesListModule(this))
                .build();

        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        refWatcher = LeakCanary.install(this);

    }

    public NetComponent getNetComponent() {
        return netComponent;
    }

    public CatchesListComponent getCatchesListComponent() {
        return catchesListComponent;
    }

    public static RefWatcher getRefWatcher(Context context) {
        BaseApplication application = (BaseApplication) context.getApplicationContext();
        return application.refWatcher;
    }

}