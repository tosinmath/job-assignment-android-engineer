package com.fishbrain.assignment.ui.details;


import android.support.annotation.NonNull;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.network.interactor.CatchesInteractor;
import com.fishbrain.android.network.utils.Logger;
import com.fishbrain.assignment.base.BasePresenter;
import com.fishbrain.assignment.ui.catcheslist.CatchesView;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CatchesDetailPresenter extends BasePresenter<DetailView> {

    private CatchesInteractor catchesInteractor;
    private CatchesView detailView;
    private final Logger logger = Logger.getLogger(getClass());

    public CatchesDetailPresenter(CatchesInteractor catchesInteractor) {
        this.catchesInteractor = catchesInteractor;
    }

    @Override
    public void attachView(DetailView detailView){
        super.attachView(detailView);
    }

    @Override
    public void detachView(){
        super.detachView();
    }

    public void getCatch(@NonNull CompositeDisposable compositeDisposable, String catchId){
        if(isViewAttached()) {
            getMvpView().showLoading();
            compositeDisposable.add(catchesInteractor.fetchCatch(catchId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));
        }
    }

    private void handleResponse(Catch catches) {
        getMvpView().setCatchDetails(catches);
    }

    private void handleError(@NonNull Throwable error) {
        logger.debug(error.getLocalizedMessage());
    }

}
