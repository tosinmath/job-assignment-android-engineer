package com.fishbrain.assignment.ui.details;


import android.support.annotation.Nullable;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.assignment.base.MvpView;

import java.util.List;

public interface DetailView extends MvpView {

    void setCatchDetails(@Nullable Catch catchDetails);

}
