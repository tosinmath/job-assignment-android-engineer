package com.fishbrain.assignment.ui.catcheslist;


import com.fishbrain.android.domain.Catch;
import com.fishbrain.assignment.base.MvpView;

import java.util.ArrayList;
import java.util.List;

public interface CatchesView extends MvpView {

    void setAdapter(List<Catch> catches, boolean loadmore);

}
