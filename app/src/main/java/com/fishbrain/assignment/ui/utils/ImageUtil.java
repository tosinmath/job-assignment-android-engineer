package com.fishbrain.assignment.ui.utils;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.fishbrain.android.R;

public class ImageUtil {
    private Context mContext;

    public ImageUtil(Context context) {
        mContext = context;
    }

    public static void displayImage(Context context, String imageResource, @NonNull ImageView imageView, int placeholder) {
        Glide.with(context)
                .load(imageResource)
                .placeholder(placeholder)
                .into(imageView);
    }
}
