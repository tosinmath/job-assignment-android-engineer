package com.fishbrain.assignment.ui.catcheslist;


import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.network.interactor.CatchesInteractor;
import com.fishbrain.android.network.service.ApiService;
import com.fishbrain.android.network.utils.Logger;
import com.fishbrain.assignment.base.BasePresenter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class CatchesListPresenter extends BasePresenter<CatchesView> {

    private CatchesInteractor catchesInteractor;
    private CatchesView catchesView;
    private final Logger logger = Logger.getLogger(getClass());
    private boolean loadmore;

    public CatchesListPresenter(CatchesInteractor catchesInteractor) {
        this.catchesInteractor = catchesInteractor;
    }

    @Override
    public void attachView(CatchesView catchesView){
        super.attachView(catchesView);
    }

    @Override
    public void detachView(){
        super.detachView();
    }

    public void getCatches(@NonNull CompositeDisposable compositeDisposable, boolean loadmore, String pageId){
        if(isViewAttached()) {
            getMvpView().showLoading();
            this.loadmore = loadmore;
            compositeDisposable.add(catchesInteractor.fetchCatches(pageId)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(this::handleResponse,this::handleError));
        }
    }

    private void handleResponse(List<Catch> catches) {
        getMvpView().setAdapter(catches, loadmore);
    }

    private void handleError(@NonNull Throwable error) {
        logger.debug(error.getLocalizedMessage());
    }

    public int getLikes(String id){
        return catchesInteractor.fetchLikes(id);
    }

    public int saveLikes(String id, int likes){
        return catchesInteractor.saveLikes(id, likes);
    }

    public int getCatchStatus(String id){
        return catchesInteractor.fetchCatchStatus(id);
    }

    public void saveCatchStatus(String id){
        catchesInteractor.saveCatchStatus(id);
    }


}
