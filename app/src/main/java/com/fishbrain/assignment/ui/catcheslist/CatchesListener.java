package com.fishbrain.assignment.ui.catcheslist;


public interface CatchesListener {

    int getLikes(String id);

    int saveLikes(String id, int likes);

    int getCatchStatus(String id);

    void saveCatchStatus(String id);

    void showSnackbarMsg(String msg);
}
