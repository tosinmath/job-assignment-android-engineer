package com.fishbrain.assignment.ui.catcheslist;


import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.assignment.R;
import com.fishbrain.assignment.ui.details.DetailActivity;
import com.fishbrain.assignment.ui.utils.ImageUtil;

import java.util.ArrayList;
import java.util.List;


public class CatchesListAdapter extends RecyclerView.Adapter<CatchesListAdapter.ViewHolder> {

    private final TypedValue mTypedValue = new TypedValue();
    private int mBackground;
    private List<Catch> catches;
    private Context context;
    CatchesListener mCallback;

    public class ViewHolder extends RecyclerView.ViewHolder {
        @NonNull
        public final View mView;
        @NonNull public final TextView description;
        @NonNull public final ImageView userImage;
        @NonNull public final TextView userName;
        @NonNull public final ImageView species;
        @NonNull public final TextView speciesType;
        @NonNull public final ImageView likeButton;
        @NonNull public final TextView likesCount;
        @NonNull public final Button removeButton;
        @NonNull public final LinearLayout catchRow;

        public ViewHolder(@NonNull View view) {
            super(view);
            mView = view;

            description = (TextView) view.findViewById(R.id.description);
            userName = (TextView) view.findViewById(R.id.user_name);
            userImage = (ImageView) view.findViewById(R.id.user_thumbnail);
            species = (ImageView) view.findViewById(R.id.big_image);
            speciesType = (TextView) view.findViewById(R.id.species_type);
            likeButton = (ImageView) view.findViewById(R.id.like_button);
            likesCount = (TextView) view.findViewById(R.id.likes_count);
            removeButton = (Button) view.findViewById(R.id.remove_button);
            catchRow = (LinearLayout) view.findViewById(R.id.catch_list);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }

    @NonNull
    public String getValueAt(int position) {
        return String.valueOf(catches.get(position).getId());
    }

    public CatchesListAdapter(@NonNull Context context, List<Catch> catches) {
        context.getTheme().resolveAttribute(R.attr.selectableItemBackground, mTypedValue, true);
        this.context = context;
        this.catches = catches;
        mBackground = mTypedValue.resourceId;
    }

    public void setCatchesListener(CatchesListener mCallback) {
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_catches, parent, false);
        view.setBackgroundResource(mBackground);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        /* Set your values */
        final Catch model = (Catch) catches.get(position);

        // Setting the adapter values
        String desc = "";
        String username = "";
        String speciesType = "";

        if(model.getDescription() != null) {
            desc = model.getDescription();
        }

        if(model.getOwner().getFirstName() != null && model.getOwner().getLastName() != null) {
            username = model.getOwner().getFirstName() + " " + model.getOwner().getLastName();
        }

        String imageResource = "";
        if (model.getOwner().getAvatar() != null) {
            imageResource = model.getOwner().getAvatar().getSizes().get(3).getUrl();
        }

        String species = "";
        if (model.getSpecies() != null) {
            species = model.getSpecies().getImage();
            speciesType = model.getSpecies().getName() + " . " + model.getSpecies().getSpecies();
        }

        holder.description.setText(desc);
        holder.userName.setText(username);
        holder.speciesType.setText(speciesType);
        ImageUtil.displayImage(holder.species.getContext(), species, holder.species, R.drawable.place_holder_big);
        ImageUtil.displayImage(holder.userImage.getContext(), imageResource, holder.userImage, R.drawable.place_holder);


        // Like button and Remove button implementation
        if(model.getRecentLikes().getTotalNumberOfLikes() != null) {
            int num = model.getRecentLikes().getTotalNumberOfLikes();
            num += mCallback.getLikes(String.valueOf(catches.get(holder.getAdapterPosition()).getId()));
            String str = num + " " + ((num > 1) ? context.getString(R.string.likes) : context.getString(R.string.like));
            holder.likesCount.setText(str);
        }

        holder.likeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int num = model.getRecentLikes().getTotalNumberOfLikes();
                int likes = mCallback.saveLikes(String.valueOf(catches.get(holder.getAdapterPosition()).getId()), num);
                String str = likes + " " + ((likes > 1) ? context.getString(R.string.likes) : context.getString(R.string.like));
                holder.likesCount.setText(str);
                mCallback.showSnackbarMsg(context.getString(R.string.snackbar_msg_like));
            }
        });

        int status = mCallback.getCatchStatus(String.valueOf(catches.get(holder.getAdapterPosition()).getId()));
        if(status != 0) {
            holder.catchRow.setVisibility(View.GONE);
            holder.catchRow.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
        } else {
            holder.catchRow.setVisibility(View.VISIBLE);
        }

        holder.removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.saveCatchStatus(String.valueOf(catches.get(holder.getAdapterPosition()).getId()));
                removeItem(holder.getAdapterPosition());
                mCallback.showSnackbarMsg(context.getString(R.string.snackbar_msg_removed));
            }
        });




        // launch the detail activity to show catches information
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(@NonNull View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra("position", holder.getAdapterPosition());
                intent.putExtra("catchId", String.valueOf(catches.get(holder.getAdapterPosition()).getId()));
                context.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return (null != catches ? catches.size() : 0);
    }

    public void addAll(@NonNull List<Catch> data){
        catches.addAll(data);
        notifyDataSetChanged();
    }

    public void add(Catch data){
        catches.add(data);
        notifyDataSetChanged();
    }

    public Catch getItemPos(int pos){
        return catches.get(pos);
    }

    public void clear(){
        catches.clear();
    }

    public void removeItem(int position) {
        catches.remove(position);
        notifyItemRemoved(position);
    }

}
