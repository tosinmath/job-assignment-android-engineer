package com.fishbrain.assignment.ui.details;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.network.utils.Logger;
import com.fishbrain.assignment.BaseApplication;
import com.fishbrain.assignment.R;
import com.fishbrain.assignment.base.BaseActivity;
import com.fishbrain.assignment.dagger.component.CatchesListComponent;
import com.fishbrain.assignment.ui.utils.ImageUtil;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.disposables.CompositeDisposable;

public class DetailActivity extends BaseActivity implements DetailView {

    @Inject
    CatchesDetailPresenter presenter;

    private CollapsingToolbarLayout collapsingToolbar;
    private Toolbar toolbar;
    private ProgressBar progressBar;
    private LinearLayout linearLayout;
    private CompositeDisposable compositeDisposable;

    private int position;
    private ImageView bigImageView, userImageView;
    private LinearLayout descLayout;
    @Nullable
    private String catchId;

    private TextView userName, speciesType, speciesName, speciesText,
            lengthText, weightText, description, gmtTimeZone, localTimeZone;

    @Override
    protected void setupActivity(CatchesListComponent component, Bundle savedInstanceState) {
        setContentView(R.layout.activity_detail);
        ((BaseApplication) getApplication()).getCatchesListComponent().inject(this);
        presenter.attachView(this);
        compositeDisposable = new CompositeDisposable();

        getBundleData();
        setupToolbar();
        initializeViews();
        loadView();
    }

    public void getBundleData(){
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            position = extras.getInt("position");
            catchId = extras.getString("catchId");
        }
    }

    public void setupToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initializeViews() {
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbar.setExpandedTitleColor(Color.TRANSPARENT);

        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        linearLayout = (LinearLayout) findViewById(R.id.detail_content);
        descLayout = (LinearLayout) findViewById(R.id.description_layout);

                userName = (TextView) findViewById(R.id.user_name);
        speciesType = (TextView) findViewById(R.id.species_type);
        speciesName = (TextView) findViewById(R.id.species_name);
        speciesText = (TextView) findViewById(R.id.species);
        lengthText = (TextView) findViewById(R.id.length);
        weightText = (TextView) findViewById(R.id.weight);
        description = (TextView) findViewById(R.id.description);
        gmtTimeZone = (TextView) findViewById(R.id.gmt_time_zone);
        localTimeZone = (TextView) findViewById(R.id.local_time_zone);
        bigImageView = (ImageView) findViewById(R.id.big_image);
        userImageView = (ImageView) findViewById(R.id.user_image);

    }

    @Override
    public void setCatchDetails(@Nullable Catch catchDetails){

        hideLoading();
        if(catchDetails != null){

            showLayout();

            // set the catch details
            String desc = "";
            String username = "";
            String sType = "";
            String sName = "";
            String specie = "";
            double length = 0;
            double weight = 0;
            String gmtTime = "";
            String localTime = "";

            if(catchDetails.getOwner() != null){
                username = catchDetails.getOwner().getFirstName()
                        + " " + catchDetails.getOwner().getLastName();
            }
            userName.setText(username);

            String userImage = "";
            if(catchDetails.getOwner().getAvatar() != null){
                userImage = catchDetails.getOwner().getAvatar().getSizes().get(3).getUrl();
            }

            if(catchDetails.getDescription() != null) desc = catchDetails.getDescription();
            description.setText(desc);
            if(desc.equals("")) descLayout.setVisibility(View.GONE);

            if(catchDetails.getSpecies() != null){
                sName = catchDetails.getSpecies().getName();
                sType = catchDetails.getSpecies().getName() + " . " + catchDetails.getSpecies().getSpecies();
                specie = catchDetails.getSpecies().getSpecies();
            }
            speciesName.setText(sName);
            speciesType.setText(sType);
            speciesText.setText(specie);
            collapsingToolbar.setTitle(sName);

            if(String.valueOf(catchDetails.getLength()) != null) length = catchDetails.getLength();
            lengthText.setText(getString(R.string.length) + " " + String.valueOf(length));

            if(String.valueOf(catchDetails.getWeight()) != null) weight = catchDetails.getWeight();
            weightText.setText(getString(R.string.weight) + " " + String.valueOf(weight));

            if(catchDetails.getDateCaught() != null) gmtTime = formatDate(catchDetails.getDateCaught().getTime());
            gmtTimeZone.setText(gmtTime);

            if(catchDetails.getDateCaught() != null) localTime = formatDate(catchDetails.getDateCaught().getTime());
            localTimeZone.setText(localTime);

            // set images
            String imageResource = "";
            if(catchDetails.getSpecies() != null) imageResource = catchDetails.getSpecies().getImage();
            ImageUtil.displayImage(this, imageResource, bigImageView, R.drawable.place_holder_big);
            ImageUtil.displayImage(this, userImage, userImageView, R.drawable.place_holder);

        }
    }

    public void loadView(){
        hideLayout();
        if(catchId != "") {
            presenter.getCatch(compositeDisposable, catchId);
        }
    }

    private String formatDate(Date dateStr){
        Date date = dateStr;
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy HH:mm aaa");
        return formatter.format(date);
    }

    public void showLoading() {
        if(progressBar != null && progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
    }

    public void hideLoading() {
        if(progressBar != null && progressBar.getVisibility() == View.VISIBLE) {
            progressBar.setVisibility(View.GONE);
        }
    }

    private void hideLayout(){
        linearLayout.setVisibility(View.GONE);
    }

    private void showLayout(){
        linearLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
