package com.fishbrain.assignment.ui.catcheslist;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.network.service.ApiService;
import com.fishbrain.android.network.utils.NetworkUtil;
import com.fishbrain.assignment.BaseApplication;
import com.fishbrain.assignment.R;
import com.fishbrain.assignment.base.BaseActivity;
import com.fishbrain.assignment.dagger.component.CatchesListComponent;
import com.squareup.leakcanary.LeakCanary;

import java.util.List;
import javax.inject.Inject;
import io.reactivex.disposables.CompositeDisposable;


/**
 * Created by dimitris on 19/07/16.
 */

public class MainActivity extends BaseActivity implements CatchesView, CatchesListener {

    @Inject ApiService apiService;
    @Inject CatchesListPresenter catchesListPresenter;
    private CompositeDisposable compositeDisposable;
    private ProgressBar progressBar;
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    private CatchesListAdapter adapter;

    private boolean userScrolled = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;
    private int page = 1;
    private RelativeLayout bottomLayout;

    @Override
    protected void setupActivity(CatchesListComponent component, Bundle savedInstanceState) {
        setContentView(R.layout.main);
        ((BaseApplication) getApplication()).getCatchesListComponent().inject(this);
        catchesListPresenter.attachView(this);
        compositeDisposable = new CompositeDisposable();

        initializeViews();
        loadView();
    }

    public void initializeViews() {
        bottomLayout = (RelativeLayout) findViewById(R.id.load_more_items);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView = (RecyclerView) findViewById(R.id.catches_recyclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
    }

    protected void loadView(){
        if(NetworkUtil.isConnected(getApplicationContext())) {
            populateList();
            implementScrollListener();
            hideOfflineSnackBar();
        } else {
            displayOfflineSnackbar();
        }
    }

    public void populateList() {
        catchesListPresenter.getCatches(compositeDisposable, false, String.valueOf(page));
    }


    public void setAdapter(@NonNull List<Catch> catches, boolean loadmore) {
        hideLoading();
        if (catches.size() > 0) {
            if(!loadmore) {
                adapter = new CatchesListAdapter(getApplicationContext(), catches);
                adapter.setCatchesListener(this);
                recyclerView.setAdapter(adapter);
            } else {
                adapter.addAll(catches);
                hideLoadMore();
            }
        }
    }

    public void implementScrollListener() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    userScrolled = true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                pastVisiblesItems = layoutManager.findFirstVisibleItemPosition();

                if (userScrolled && (visibleItemCount + pastVisiblesItems) == totalItemCount) {
                    userScrolled = false;
                    page = page + 1;
                    catchesListPresenter.getCatches(compositeDisposable, true, String.valueOf(page));
                    showLoadMore();
                }
            }
        });
    }

    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    public void showLoadMore(){
        bottomLayout.setVisibility(View.VISIBLE);
    }

    public void hideLoadMore(){
        bottomLayout.setVisibility(View.GONE);
    }

    @Override
    public int getLikes(String id) {
        return catchesListPresenter.getLikes(id);
    }

    @Override
    public int saveLikes(String id, int likes) {
        return catchesListPresenter.saveLikes(id, likes);
    }

    @Override
    public int getCatchStatus(String id) {
        return catchesListPresenter.getCatchStatus(id);
    }

    @Override
    public void saveCatchStatus(String id) {
        catchesListPresenter.saveCatchStatus(id);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        compositeDisposable.clear();
    }
}
