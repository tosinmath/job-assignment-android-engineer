package com.fishbrain.assignment.dagger.module;


import android.support.annotation.NonNull;

import com.fishbrain.android.network.service.ApiService;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class RetrofitModule {

    @Provides
    public ApiService providesApiService(@NonNull Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }
}