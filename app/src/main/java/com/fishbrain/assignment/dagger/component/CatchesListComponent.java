package com.fishbrain.assignment.dagger.component;


import com.fishbrain.assignment.dagger.module.CatchesListModule;
import com.fishbrain.assignment.dagger.module.RetrofitModule;
import com.fishbrain.assignment.dagger.scope.UserScope;
import com.fishbrain.assignment.ui.catcheslist.MainActivity;
import com.fishbrain.assignment.ui.details.DetailActivity;

import dagger.Component;

@UserScope
@Component(dependencies = NetComponent.class, modules = {RetrofitModule.class, CatchesListModule.class})
public interface CatchesListComponent {

    void inject(MainActivity mainActivity);
    void inject(DetailActivity detailActivity);
}