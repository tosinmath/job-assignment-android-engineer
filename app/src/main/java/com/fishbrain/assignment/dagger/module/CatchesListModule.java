package com.fishbrain.assignment.dagger.module;


import android.app.Application;
import android.support.annotation.NonNull;

import com.fishbrain.android.network.interactor.CatchesInteractor;
import com.fishbrain.android.network.interactor.CatchesInteractorImpl;
import com.fishbrain.android.network.service.ApiService;
import com.fishbrain.android.providers.DbProvider;
import com.fishbrain.android.repository.CatchesRepository;
import com.fishbrain.assignment.ui.catcheslist.CatchesListPresenter;
import com.fishbrain.assignment.ui.details.CatchesDetailPresenter;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;

@Module
public class CatchesListModule {

    private Application application;
    //@Inject ApiService apiService;

    public CatchesListModule(Application application){
        this.application = application;
    }

    @NonNull
    @Provides
    public CatchesListPresenter getCatchesListPresenter(CatchesInteractor catchesInteractor){
        return new CatchesListPresenter(catchesInteractor);
    }

    @NonNull
    @Provides
    CatchesInteractor provideCatchesInteractor(ApiService apiService, CatchesRepository catchesRepository) {
        return new CatchesInteractorImpl(apiService, catchesRepository);
    }

    @NonNull
    @Provides
    CatchesRepository provideRepository(DbProvider dbProvider) {
        return new CatchesRepository( dbProvider );
    }

    @NonNull
    @Provides
    public CatchesDetailPresenter getCatchesDetailPresenter(CatchesInteractor catchesInteractor){
        return new CatchesDetailPresenter(catchesInteractor);
    }

}