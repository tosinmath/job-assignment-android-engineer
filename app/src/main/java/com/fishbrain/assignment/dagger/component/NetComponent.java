package com.fishbrain.assignment.dagger.component;


import android.content.SharedPreferences;

import com.fishbrain.android.providers.DbProvider;
import com.fishbrain.assignment.dagger.module.AppModule;
import com.fishbrain.assignment.dagger.module.NetModule;
import javax.inject.Singleton;

import dagger.Component;
import io.realm.Realm;
import retrofit2.Retrofit;

@Singleton
@Component(modules={AppModule.class, NetModule.class})
public interface NetComponent {

    // downstream components need these exposed
    Retrofit restAdapter();
    SharedPreferences sharedPreferences();
    DbProvider getDbProvider();

}
