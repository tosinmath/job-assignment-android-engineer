package com.fishbrain.android.network.interactor;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.domain.CatchStatus;
import com.fishbrain.android.domain.Likes;
import com.fishbrain.android.network.service.ApiService;
import com.fishbrain.android.repository.CatchesRepository;

import java.util.List;

import io.reactivex.Observable;
import io.realm.RealmResults;

/**
 * The implementation of {@link CatchesInteractor}
 * <p>
 * Created by dimitris.lachanas on 22/08/15.
 */
public class CatchesInteractorImpl implements CatchesInteractor {

	private final static String VERBOSITY = "3";
	private ApiService apiService;
	private CatchesRepository catchesRepository;

	public CatchesInteractorImpl(ApiService apiService, CatchesRepository catchesRepository) {
		this.apiService = apiService;
		this.catchesRepository = catchesRepository;
	}

	@Override
	public Observable<List<Catch>> fetchCatches(String pageId){
		return apiService.getCatches(pageId, VERBOSITY)
		.flatMap(catches -> Observable.just(catches));
	}

	@Override
	public Observable<Catch> fetchCatch(String catchId){
		return apiService.getCatch(catchId, Integer.parseInt(VERBOSITY))
				.flatMap(catches -> Observable.just(catches));
	}

	@Override
	public int fetchLikes(String id){
		RealmResults<Likes> results = catchesRepository.getLikesData(id);
		int likes = ((results.size() != 0) ? results.first().getLikes() : 0);
		return likes;
	}

	@Override
	public int saveLikes(String id, int likes){
		return catchesRepository.saveLikesData(id, likes);
	}

	@Override
	public int fetchCatchStatus(String id){
		RealmResults<CatchStatus> results = catchesRepository.getStatusData(id);
		int status = ((results.size() != 0) ? results.first().getStatus() : 0);
		return status;
	}

	@Override
	public void saveCatchStatus(String id){
		catchesRepository.saveStatusData(id);
	}

}
