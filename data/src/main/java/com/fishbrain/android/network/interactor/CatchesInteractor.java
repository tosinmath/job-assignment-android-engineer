package com.fishbrain.android.network.interactor;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.network.service.ApiService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * This is an interactor used for triggering the network calls in order to fetch the catch(es)
 * Created by dimitris.lachanas on 22/08/15.
 */
public interface CatchesInteractor {

    /**
     * This method is used for fetching the catches from the API
     *
     * @param pageId    the pageId that we want to fetch from the API
     */
    Observable<List<Catch>> fetchCatches(String pageId);

    Observable<Catch> fetchCatch(String catchId);

    int fetchLikes(String id);

    int saveLikes(String id, int likes);

    int fetchCatchStatus(String id);

    void saveCatchStatus(String id);


}
