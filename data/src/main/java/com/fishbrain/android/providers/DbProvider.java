package com.fishbrain.android.providers;


import android.app.Application;
import android.support.annotation.NonNull;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class DbProvider  {

    @Inject
    public DbProvider(@NonNull Application application) {
        Realm.init(application);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .name("catches.realm")
                .deleteRealmIfMigrationNeeded()
                .schemaVersion(1)
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }


    public Realm getDatabase() {
        return Realm.getDefaultInstance();
    }
}
