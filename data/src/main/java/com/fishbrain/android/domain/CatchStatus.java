package com.fishbrain.android.domain;


import io.realm.RealmObject;

public class CatchStatus extends RealmObject {

    private String id;
    private int status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
