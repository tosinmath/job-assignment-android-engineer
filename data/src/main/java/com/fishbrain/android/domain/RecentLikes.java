package com.fishbrain.android.domain;


import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class RecentLikes {

    private String type;
    @SerializedName("total_number_of_likes")
    private Integer totalNumberOfLikes;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getTotalNumberOfLikes() {
        return totalNumberOfLikes;
    }

    public void setTotalNumberOfLikes(Integer totalNumberOfLikes) {
        this.totalNumberOfLikes = totalNumberOfLikes;
    }
}
