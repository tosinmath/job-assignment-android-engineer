package com.fishbrain.android.domain;


import io.realm.RealmObject;

public class Likes extends RealmObject {

    private String id;
    private int likes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }
}
