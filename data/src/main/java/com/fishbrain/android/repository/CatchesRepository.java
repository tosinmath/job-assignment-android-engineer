package com.fishbrain.android.repository;



import android.util.Log;

import com.fishbrain.android.domain.Catch;
import com.fishbrain.android.domain.CatchStatus;
import com.fishbrain.android.domain.Likes;
import com.fishbrain.android.providers.DbProvider;

import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmResults;

public class CatchesRepository {

    private DbProvider dbProvider;

    @Inject
    public CatchesRepository(DbProvider dbProvider) {
        this.dbProvider = dbProvider;
    }

    public RealmResults<Likes> getLikesData(String id){
        return getRealm().where(Likes.class).equalTo("id", id).findAll();
    }

    public int saveLikesData(String id, int likesNum){
        likesNum += 1;
        RealmResults<Likes> results = getLikesData(id);
        getRealm().beginTransaction();
        if(results.size() == 0) {
            Likes likes = getRealm().createObject(Likes.class);
            likes.setId(id);
            likes.setLikes(likesNum);
        } else {
            results = getLikesData(id);
            likesNum += results.first().getLikes();
            results.first().setLikes(likesNum);
        }
        getRealm().commitTransaction();
        return likesNum;
    }

    public RealmResults<CatchStatus> getStatusData(String id){
        return getRealm().where(CatchStatus.class).equalTo("id", id).findAll();
    }

    public void saveStatusData(String id){
        RealmResults<CatchStatus> results = getStatusData(id);
        getRealm().beginTransaction();
        if(results.size() == 0) {
            CatchStatus catchStatus = getRealm().createObject(CatchStatus.class);
            catchStatus.setId(id);
            catchStatus.setStatus(1);
        }
        getRealm().commitTransaction();
    }

    private Realm getRealm() {
        return dbProvider.getDatabase();
    }

}
